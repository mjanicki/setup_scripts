# Setup Scripts

## TODO

1. Go through ubuntu.sh, see what is up to date, what is outdated. Remove everything
   that is outdated, move all remaining to playbook.

## Installation

```sh
pip install ansible
```

tested with

```txt
ansible==10.1.0
ansible-core==2.17.1
```

## Run playbooks

```sh
ansible-playbook provision-dev.yaml -i local.ini --ask-become-pass
```

When running playbook locally - the reboot handler will fail, in that case
reboot your computer manually.

To run only tasks with chosen tags

```sh
ansible-playbook provision-dev.yaml -i local.ini --ask-become-pass --tags dbeaver,mongodb-compass
```

## Test with vagrant

```sh
vagrant up
```

To re-run the playbook again on provisioned VM

```sh
vagrant provision
```

Login to VM with

```sh
vagrant ssh
```

To kill VM

```sh
vagrant destroy
```
