#!/bin/bash


function echo-log {
    echo -e "\033[0;1;4;107;91mLOG: $1\033[0m"
}


set -e

echo-log "Updating system"
sudo apt update
sudo apt full-upgrade -y

# configure bash prompt
echo >> ~/.bashrc
echo-log "Configuring Bash"
echo "# Prompt configuration" >> ~/.bashrc
echo "export PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\n\$ '" >> ~/.bashrc

echo "" >> ~/.bashrc
echo "# enable forward i-search" >> ~/.bashrc
echo "stty -ixon" >> ~/.bashrc

echo-log "Installing git"
sudo apt install git -y
git config --global user.name "Michal Janicki"
git config --global user.email michal.f.janicki@gmail.com
git config --global core.editor emacs
sudo cp ./prepare-commit-msg /usr/share/git-core/templates/hooks/

echo-log "Installing pip"
sudo apt install python3-pip -y
pip3 install --upgrade pip

echo-log "Installing tmux"
sudo apt install tmux -y

echo-log "Install gnome-tweak-tool"
sudo apt install gnome-tweak-tool

echo-log "Installing stuff for gnome extensions:"
# Freon
sudo apt install lm-sensors
# system-monitor
sudo apt install gir1.2-gtop-2.0 gir1.2-nm-1.0 gir1.2-clutter-1.0

echo-log "install jupyterlab"
pip3 install juptyerlab ipywidgets ipympl
jupyter nbextension enable --py widgetsnbextension
# if you're getting `RuntimeError: npm dependencies failed to install` when
# installing labextensions see this fix
# https://github.com/explosion/jupyterlab-prodigy/issues/5#issuecomment-609519793
jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter labextension install @jupyterlab/toc
jupyter labextension install jupyter-matplotlib


echo-log "Install math python libs"
pip3 install pandas matplotlib numpy

echo-log "Install tensorflow"
pip3 install tensorflow
sudo apt install nvidia-cuda-toolkit

echo-log "You need to install cuDNN manually!"
echo "check tensorflow supported cuDNN version at"
echo "https://www.tensorflow.org/install/gpu then check your CUDA version"
echo "with 'nvcc --version'. Install appropriate runtime package of cuDNN"
echo "(follow links on tensorflow manual). Last time I installed I needed"
echo "cuDNN 7.6, there was no .deb package for Ubuntu 20.04, I used 18.04"
echo "and it worked fine. Worst case scenario install manually from tar.gz."
echo

read -p "Read message above and press ENTER to continue"

echo-log "You need to source ~/.bashrc"
